<?php

namespace AppBundle\EventListener;

class SidebarListener {

    /**
     * @return EventDispatcher
     */
    protected function getDispatcher() {
        return $this->get('event_dispatcher');
    }

}
