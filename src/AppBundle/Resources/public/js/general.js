$(document).ready(function () {

    $(".menu_right_lk a i").click(function (e) {
        e.stopPropagation();
        if ($(this).hasClass('fa-bars')) {
            $('.menu_right').show('slow');
            $(this).removeClass('fa-bars').addClass('fa-close');
        } else {
            $('.menu_right').hide('slow');
            $(this).addClass('fa-bars').removeClass('fa-close');
        }
    });

    $(".forum_msg").mCustomScrollbar({
        theme: "dark"
    });

    $(".logo a").click(function (e) {
        e.stopPropagation();
        var img = $(this).find('img');
        if ($(this).hasClass('bg')) {
            img.attr("src", "bundles/app/images/trino-15-sm.png");
            $('.col_left').addClass('col_left_min');
            $(this).removeClass('bg').addClass('sm');
            $('.logo').addClass('logo_sm');
            $('.header_right').addClass('hr_sm');
            $('.col_middle').addClass('cm_sm');
        } else {
            img.attr("src", "bundles/app/images/trino-15.png");
            $('.col_left').removeClass('col_left_min');
            $(this).addClass('bg').removeClass('sm');
            $('.logo').removeClass('logo_sm');
            $('.header_right').removeClass('hr_sm');
            $('.col_middle').removeClass('cm_sm');
        }
    });

    function setDateTime() {
        function updateClock() {
            var myDate = new Date();

            let daysList = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            let monthsList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Aug', 'Oct', 'Nov', 'Dec'];


            let date = myDate.getDate();
            let month = monthsList[myDate.getMonth()];
            let year = myDate.getFullYear();
            let day = daysList[myDate.getDay()];

            let today = `${date} ${month} ${year}, ${day}`;

            let amOrPm;
            let twelveHours = function () {
                if (myDate.getHours() > 12)
                {
                    amOrPm = 'PM';
                    let twentyFourHourTime = myDate.getHours();
                    let conversion = twentyFourHourTime - 12;
                    return `${conversion}`;

                } else {
                    amOrPm = 'AM';
                    return `${myDate.getHours()}`;
                }
            };
            let hours = twelveHours();
            let minutes = myDate.getMinutes();
            let currentTime = `${hours}:${minutes} ${amOrPm}`;

            document.getElementById('base-current-time').innerHTML = currentTime + "<span>" + today + "</span>";
        }
        setInterval(updateClock, 1000);
    }

    setDateTime();
});