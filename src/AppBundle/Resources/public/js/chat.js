$(function () {
    //make connection
    var socket = io.connect('http://192.168.0.133:4000')

    //buttons and inputs
    var message = $("#message");
    var username = document.getElementById('username').innerHTML;
    var send_message = $("#send_message");
    var chatroom = $("#chatroom");
    var feedback = $("#feedback");
    
    //Emit message and username
    var user = false;
    send_message.click(function () {
        if(!user) {
            socket.emit('change_username', {username: username});
            user = true;
        }
        socket.emit('new_message', {message: message.val()});
    });

    //Listen on new_message
    socket.on("new_message", (data) => {
        feedback.html('');
        message.val('');
        chatroom.append("<p class='message'>" + data.username + ": " + data.message + "</p>");
    });

    //Emit typing
    message.bind("keypress", () => {
        socket.emit('typing');
    });

    //Listen on typing
    socket.on('typing', (data) => {
        feedback.html("<p><i>" + data.username + " is typing a message..." + "</i></p>");
    });
});

