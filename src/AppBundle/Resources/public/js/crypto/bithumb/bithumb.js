/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Bithumb {

    constructor() {
        this._baseUrl = "http://192.168.0.133:335";
    }

    geto(symbol, url) {
        return {'path': url, 'symbol': symbol};
    }

    timer(time, open) {
        window.setInterval(function () {
            open();
        }, time);
    }

    _setupWebSocket(eventHandler, arr) {

        var socket = new io(arr['path']);
        socket.emit('bithumb', {
            symbol: arr['symbol']
        });

        socket.on(arr['symbol'], function (msg) {
            eventHandler(msg);
        });

        return socket;
        
    }

    onTicker(symbol, eventHandler) {
        return this._setupWebSocket(eventHandler, this.geto(symbol.toLowerCase(), this._baseUrl));
    }
}