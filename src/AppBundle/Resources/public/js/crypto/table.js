/* Created by Dragos version 1.0 */

class Table {

    constructor(id) {
        this._id = id;
    }

    add(count, classArr) {
        var table = document.getElementById(this._id);
        var row = table.insertRow(-1);
        row.className = classArr[0];
        var c = row.insertCell(0);
        for (var i = 1; i <= count; i++) {
            if (typeof classArr[i] !== 'undefined') {
                c.className = classArr[i];
            }
            c = row.insertCell(i);
            c.innerHTML = '';
        }
    }

    remove() {
        document.getElementById(this._id).deleteRow(-1);
    }

    edit(content, line, column, classTr, classTd) {
        var x = document.getElementById(this._id).rows[line].cells;

        if (typeof classTd !== 'undefined') {
            document.getElementById(this._id).rows[line].cells[column].classList.remove(document.getElementById(this._id).rows[line].cells[column].className);
            document.getElementById(this._id).rows[line].cells[column].classList.add(classTd);
        }

        x[column].innerHTML = content;
    }

    get(line, column) {
        var x = document.getElementById(this._id).rows[line].cells;
        return x[column].innerHTML;
    }

    count() {
        var x = $('#' + this._id + ' td').closest('tr').length;
        return x;
    }

    getColumns() {
        return document.getElementById(this._id).rows[0].cells.length;
    }

    check(content, line, column) {
        var x = document.getElementById(this._id).rows[line].cells;

        if (line > count())
            return false;

        if (x[column].innerHTML === content) {
            return true;
        }

        return false;
    }
}