/* Auto-update table for each cryptocurrency */

/* Market table */
function addBinanceMarketSymbol(id, symbol, pos) {
    var tb = new Table(id);
    var binance = new Binance();
    var classArr = ['listContent', 't_wh', 't_wh', 't_wh', 't_wh', 't_wh', 't_wh', 't_wh'];
    tb.add(tb.getColumns(), classArr);
    binance.onTicker(symbol, function (msg) {
        var content = [msg['s'], msg['x'], "null", msg['p'], msg['l'], "null", msg['h']];
        for (var i = 0; i < content.length; i++) {
            switch (i) {
                case 3:
                    tb.edit(content[i], pos, i, null, parseFloat(msg['p']) < 0 ? 't_dw' : 't_up');
                    break;

                default:
                    tb.edit(content[i], pos, i);
                    break;
            }
        }
    });
}

function cryptoMarketTable() {
    $.getScript('bundles/app/js/crypto/table.js', function () {
        $.getScript('bundles/app/js/crypto/binance/binance.js', function () {
            $.getScript('bundles/app/js/crypto/bitfinex/bitfinex.js', function () {
                $.getScript('bundles/app/js/crypto/huobi/huobi.js', function () {
                    $.getScript('bundles/app/js/crypto/okex/okex.js', function () {
                        $.getScript('bundles/app/js/crypto/bithumb/bithumb.js', function () {

                            var binanceSymbols = ['BTCUSDT', 'TUSDETH', 'ETCETH', 'ETHBTC', 'ADAETH', 'LTCETH', 'VETETH'];

                            // Manager Makert Table
                            try {
                                // Binance symbols
                                for (var i = 0; i < binanceSymbols.length; i++) {
                                    addBinanceMarketSymbol('market_table', binanceSymbols[i], i + 1);
                                }

                                $(document).click(function () {
                                    
                                    $('.col_middle_max').css({'width': '100%'});
                                    $('.market_bar').hide();
                                    
                                });

                                $('#market_table .listContent').click(function (event) {
                                    $target = $(event.target);
                                    if (!$target.closest('.market_bar').length && $('.market_bar').is(":visible")) {
                                        event.stopPropagation();
                                    } else {
                                        $('.col_middle_max').css({'width': 'calc(100% - 322px)'});
                                        $('.market_bar').show();
                                        event.stopPropagation();
                                    }
                                });

                            } catch (err) {
                            }
                        });

                    });
                });
            });
        });
    });
}

/* init function */
(function ($) {
    cryptoMarketTable();
}(jQuery));