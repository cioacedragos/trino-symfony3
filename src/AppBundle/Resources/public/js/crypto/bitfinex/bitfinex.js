/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Bitfinex {

    constructor() {
        this._baseUrl = 'wss://api.bitfinex.com/ws/';
    }
    
    geto(symbol, url){
        return {'path' : url, 'symbol' : symbol};
    }

    _setupWebSocket(eventHandler, arr) {
        const ws = new WebSocket(arr['path']);
        
        ws.onopen = function open(message){
            ws.send(JSON.stringify({
                'event' : 'subscribe',
                'channel' : 'ticker',
                'pair' : arr['symbol']
            }));
        };

        ws.onmessage = function open(message) {
            let event = JSON.parse(message.data);
            if(event[1] !== 'hb'){
                eventHandler(event);
            }
        };

        ws.onerror = function (msg) {
            /* Error message */
        };

        return ws;
    }

    onDepthUpdate(symbol, eventHandler) {
        return this._setupWebSocket(eventHandler, this.geto(symbol, this._baseUrl));
    }

    onDepthLevelUpdate(symbol, level, eventHandler) {
        return this._setupWebSocket(eventHandler, this.geto(symbol, this._baseUrl));
    }

    onKline(symbol, interval, eventHandler) {
        return this._setupWebSocket(eventHandler, this.geto(symbol, this._baseUrl));
    }

    onAggTrade(symbol, eventHandler) {
        return this._setupWebSocket(eventHandler, this.geto(symbol, this._baseUrl));
    }

    onTrade(symbol, eventHandler) {
        return this._setupWebSocket(eventHandler, this.geto(symbol, this._baseUrl));
    }

    onTicker(symbol, eventHandler) {
        return this._setupWebSocket(eventHandler, this.geto(symbol, this._baseUrl));
    }

    onAllTickers(eventHandler) {
        return this._setupWebSocket(eventHandler, this.geto(null, this._baseUrl));
    }
    
}