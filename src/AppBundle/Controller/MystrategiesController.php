<?php

namespace AppBundle\Controller;

use Avanzu\AdminThemeBundle\Event\ThemeEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Response;

class MystrategiesController extends Controller {

    /**
     * @return EventDispatcher
     */
    protected function getDispatcher() {
        return $this->get('event_dispatcher');
    }

    public function showAction() {
        
        $entityManager = $this->getDoctrine()->getManager();
        $strategies = $entityManager->getRepository("AppBundle:Strategies")->findAll();
        if (!$this->getDispatcher()->hasListeners(ThemeEvents::THEME_NAVBAR_USER)) {
            return new Response();
        }

        return $this->render('pages/mystrategies.html.twig', array(
            'strategies' => $strategies
        ));
    }

}