<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DashboardFundsController extends Controller {

    /**
     * @Route("/def_funds", name="link_funds")
     * @return RedirectResponse
     */
    public function indexAction() {
        return new RedirectResponse($this->generateUrl('user_funds'));
    }

    /**
     * @Route("/funds", name="user_funds")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fundsAction() {
        return $this->render('dashboard/dashboard_funds.html.twig');
    }
}
