<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DashboardStrategiesController extends Controller {

    /**
     * @Route("/def_strategies", name="link_strategies")
     * @return RedirectResponse
     */
    public function indexAction() {
        return new RedirectResponse($this->generateUrl('user_strategies'));
    }

    /**
     * @Route("/strategies", name="user_strategies")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function strategiesAction() {
        return $this->render('dashboard/dashboard_strategies.html.twig');
    }
}
