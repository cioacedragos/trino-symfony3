<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DashboardMarketsController extends Controller {

    /**
     * @Route("/", name="homepage")
     * @return RedirectResponse
     */
    public function indexAction() {
        return new RedirectResponse($this->generateUrl('markets_dashboard'));
    }

    /**
     * @Route("/markets", name="markets_dashboard")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction() {
        $entityManager = $this->getDoctrine()->getManager();
        
        $user = $entityManager->getRepository("SalexUserBundle:User")->findAll();
        return $this->render('dashboard/dashboard_markets.html.twig', array(
            'User' => $user
        ));
    }

}
