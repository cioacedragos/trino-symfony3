<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DashboardNewsController extends Controller {

    /**
     * @Route("/def_news", name="link_news")
     * @return RedirectResponse
     */
    public function indexAction() {
        return new RedirectResponse($this->generateUrl('user_news'));
    }

    /**
     * @Route("/news", name="user_news")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function strategiesAction() {
        return $this->render('dashboard/dashboard_news.html.twig');
    }
}
