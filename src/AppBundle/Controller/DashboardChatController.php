<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DashboardChatController extends Controller
{

    /**
     * @Route("/def_chat", name="link_chat")
     * @return RedirectResponse
     */
    public function indexAction()
    {
        return new RedirectResponse($this->generateUrl('user_chat'));
    }

    /**
     * @Route("/chat", name="user_chat")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fundsAction()
    {
        return $this->render('dashboard/dashboard_chat.html.twig');
    }
}
