<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Strategies;

class AddmystrategiesController extends Controller {

    /**
     * @Route("/def_addmystrategies", name="link_addmystrategies")
     * @return RedirectResponse
     */
    public function indexAction() {
        return new RedirectResponse($this->generateUrl('user_addmystrategies'));
    }

    /**
     * @Route("/addmystrategies", name="user_addmystrategies")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addmystrategiesAction() {
        return $this->render('widgets/mystrategies_add.html.twig');
    }
    
     /**
     * @Route("/execaddstrategy", name="exec_addmystrategies")
     */
    public function execaddmystrategies(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        
        $postData = $request->request->all();
        
        if ($postData) {
            
            $strategy = new Strategies();
            $strategy->setName($postData["strategy_name"]);
            $strategy->setCreatedAt(new \DateTime());
            $strategy->setStartDate(new \DateTime($postData["strategy_start"]));
            
            switch($postData["strategy_status"]) {
                case 'Active':
                    $status = 1;
                    break;
                case 'Disable':
                    $status = 2;
                    break;
                case 'Paper':
                    $status = 3;
                    break;
                default:
                    $status = 1;
                    break;
            }
            $strategy->setStatus($status);
            $strategy->setSubscribers(0);
            $strategy->setExecuted(0);
            $strategy->setOutstanding(0);
            $strategy->setOffered(0);
            $strategy->setAvailable(0);
            $strategy->setLastPrice($postData["strategy_price"]);
            $strategy->setExecutions($postData["strategy_executions"]);
            $strategy->setExecutionsLevel($postData["strategy_executions_level"]);
            
            $entityManager->persist($strategy);
            $entityManager->flush();
        }
        
        return $this->render('dashboard/dashboard_strategies.html.twig');
    }

}
