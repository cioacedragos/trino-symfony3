<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DashboardMystrategiesController extends Controller {

    /**
     * @Route("/def_mystrategies", name="link_mystrategies")
     * @return RedirectResponse
     */
    public function indexAction() {
        return new RedirectResponse($this->generateUrl('user_mystrategies'));
    }

    /**
     * @Route("/mystrategies", name="user_mystrategies")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mystrategiesAction() {
        return $this->render('dashboard/dashboard_mystrategies.html.twig');
    }
}