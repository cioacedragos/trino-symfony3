<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EditmystrategiesController extends Controller {

    /**
     * @Route("/def_editmystrategies", name="link_editmystrategies")
     * @return RedirectResponse
     */
    public function indexAction() {
        return new RedirectResponse($this->generateUrl('user_editmystrategies'));
    }

    /**
     * @Route("/editmystrategies", name="user_editmystrategies")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editmystrategiesAction() {
        return $this->render('widgets/mystrategies_edit.html.twig');
    }
}