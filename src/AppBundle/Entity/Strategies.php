<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Strategies
 *
 * @ORM\Table(name="strategies")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StrategiesRepository")
 */
class Strategies {

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLE = 2;
    const STATUS_PAPER = 3;

    public static $statuses = array(
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_DISABLE => 'Disable',
        self::STATUS_PAPER => 'Paper'
    );

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CreatedAt", type="date")
     */
    private $createdAt;

    /**
     * @var int
     *
     * @ORM\Column(name="Status", type="integer")
     */
    private $status = self::STATUS_ACTIVE;

    /**
     * @var int
     *
     * @ORM\Column(name="Subscribers", type="integer")
     */
    private $subscribers;

    /**
     * @var int
     *
     * @ORM\Column(name="Executed", type="integer")
     */
    private $executed;

    /**
     * @var int
     *
     * @ORM\Column(name="Outstanding", type="integer")
     */
    private $outstanding;

    /**
     * @var int
     *
     * @ORM\Column(name="Offered", type="integer")
     */
    private $offered;

    /**
     * @var int
     *
     * @ORM\Column(name="Available", type="integer")
     */
    private $available;

    /**
     * @var float
     *
     * @ORM\Column(name="LastPrice", type="float")
     */
    private $lastPrice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="StartDate", type="date")
     */
    private $startDate;

    /**
     * @var string
     *
     * @ORM\Column(name="Executions", type="string", length=255)
     */
    private $executions;

    /**
     * @var string
     *
     * @ORM\Column(name="ExecutionsLevel", type="string", length=255)
     */
    private $executionsLevel;
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Strategies
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Strategies
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Strategies
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set subscribers
     *
     * @param integer $subscribers
     *
     * @return Strategies
     */
    public function setSubscribers($subscribers) {
        $this->subscribers = $subscribers;

        return $this;
    }

    /**
     * Get subscribers
     *
     * @return int
     */
    public function getSubscribers() {
        return $this->subscribers;
    }

    /**
     * Set executed
     *
     * @param integer $executed
     *
     * @return Strategies
     */
    public function setExecuted($executed) {
        $this->executed = $executed;

        return $this;
    }

    /**
     * Get executed
     *
     * @return int
     */
    public function getExecuted() {
        return $this->executed;
    }

    /**
     * Set outstanding
     *
     * @param integer $outstanding
     *
     * @return Strategies
     */
    public function setOutstanding($outstanding) {
        $this->outstanding = $outstanding;

        return $this;
    }

    /**
     * Get outstanding
     *
     * @return int
     */
    public function getOutstanding() {
        return $this->outstanding;
    }

    /**
     * Set offered
     *
     * @param integer $offered
     *
     * @return Strategies
     */
    public function setOffered($offered) {
        $this->offered = $offered;

        return $this;
    }

    /**
     * Get offered
     *
     * @return int
     */
    public function getOffered() {
        return $this->offered;
    }

    /**
     * Set available
     *
     * @param integer $available
     *
     * @return Strategies
     */
    public function setAvailable($available) {
        $this->available = $available;

        return $this;
    }

    /**
     * Get available
     *
     * @return int
     */
    public function getAvailable() {
        return $this->available;
    }

    /**
     * Set lastPrice
     *
     * @param float $lastPrice
     *
     * @return Strategies
     */
    public function setLastPrice($lastPrice) {
        $this->lastPrice = $lastPrice;

        return $this;
    }

    /**
     * Get lastPrice
     *
     * @return float
     */
    public function getLastPrice() {
        return $this->lastPrice;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Strategies
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set executions
     *
     * @param string $executions
     *
     * @return Strategies
     */
    public function setExecutions($executions) {
        $this->executions = $executions;

        return $this;
    }

    /**
     * Get executions
     *
     * @return string
     */
    public function getExecutions() {
        return $this->executions;
    }

    /**
     * Set executionsLevel
     *
     * @param string $executionsLevel
     *
     * @return Strategies
     */
    public function setExecutionsLevel($executionsLevel) {
        $this->executionsLevel = $executionsLevel;

        return $this;
    }

    /**
     * Get executionsLevel
     *
     * @return string
     */
    public function getExecutionsLevel() {
        return $this->executionsLevel;
    }

}
