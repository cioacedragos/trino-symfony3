<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Favourite
 *
 * @ORM\Table(name="favourite")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FavouriteRepository")
 */
class Favourite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="news", type="integer")
     */
    private $news;

    /**
     * @var int
     *
     * @ORM\Column(name="user", type="integer")
     */
    private $user;

    /**
     * @var bool
     *
     * @ORM\Column(name="shared", type="boolean")
     */
    private $shared;

    /**
     * @var bool
     *
     * @ORM\Column(name="liked", type="boolean")
     */
    private $liked;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set news
     *
     * @param integer $news
     *
     * @return Favourite
     */
    public function setNews($news)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return int
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Favourite
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set shared
     *
     * @param boolean $shared
     *
     * @return Favourite
     */
    public function setShared($shared)
    {
        $this->shared = $shared;

        return $this;
    }

    /**
     * Get shared
     *
     * @return bool
     */
    public function getShared()
    {
        return $this->shared;
    }

    /**
     * Set liked
     *
     * @param boolean $liked
     *
     * @return Favourite
     */
    public function setLiked($liked)
    {
        $this->liked = $liked;

        return $this;
    }

    /**
     * Get liked
     *
     * @return bool
     */
    public function getLiked()
    {
        return $this->liked;
    }
}

